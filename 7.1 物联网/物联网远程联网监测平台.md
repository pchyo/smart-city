- [物联网远程联网监测平台 (qq.com)](https://mp.weixin.qq.com/s/Ab-ssTcNM6oBRkslkgKpYg)

**消防发展现状**

近年来，火灾事故发生的数量呈逐年上涨趋势，消防安全管理工作的重要性日益明显。特别是社区、商场、饭店等人员密集场所，多为火灾高发地区，消防设备设施没有得到有效监管，很多消防隐患无法及时察觉，最终酿成大祸。群众消防意识比较淡薄，基层管理者监测方法、可用资源有限，传统模式已经跟不上快速发展的时代要求，致使消防设施无序发展，时间越久消防设施将暴露越多的问题。

**系统介绍**

智慧消防安全管理系统以建筑物内的消防设施为感知对象，将离散在城市各个建筑内部的火灾自动报警设备、消防灭火系统、电气火灾系统、防火分隔系统、应急疏散系统、视频监控系统、无线烟感等建筑消防设施全部感知起来，利用强大的计算机软件系统，进行查询、处理、统计、分析，从而实现对联网单位建筑消防设施的全面、远程、集中监控管理，并集成维保巡查、消防在线学习等多样化功能，以保证单位消防安全，提高消防安全水平。

**1.****火灾自动报警系统**

火灾自动报警系统通过用户信息传输装置采集不同报警主机协议数据并上传云平台，实现对报警信息的实时管控。

实时监测的报警信息包括国标七类信息：火警、反馈、故障、屏蔽、监管、复位、启动。

**2.****消防灭火系统**

对各类设备系统进行24h自动监控和集中远程管理；同时可远程控制、指挥现场设施、设备的状况，降低对现场工作人员技术要求的同时保证服务质量；从而降低公司成本，提高项目综合管理能力。

**3.****电气火灾系统**

数据显示，现如今的火灾事故有40%左右都是由电器火灾引起的。所以我们对配电箱的电压、电流、剩余电流、线缆温度以及故障电弧进行了监测。从根源预防火灾的发生。

**4.****防火分隔系统**

**5.****应急疏散系统**

针对外访人员多，建筑体结构陈旧，一旦发生火灾或其他事件，人员难有序疏散，极易造成伤亡及损失事故，本平台智能疏散系统，一旦发生情况疏散指示灯自动指向安全出口，通过平台数据运算统计现场人员数量及楼层分布构造，合理给出最佳路线。

通过平台端及移动接收到到告警信息。消控室人员调取现场摄像头判断确认为真实火警后，需启动人员疏散程序后，启动智能疏散系统。

**6.****视频监控系统**

视频监控接入查看，单位探测器动作后可点击联动报警区域相关视频，支持视频截图。

**7.****无线烟感**

NB-IoT技术特点：具有低功耗，连接数量大，低时耗，网络覆盖广等特点，采用中国电信强大的信息传输技术，精准的对室内烟雾浓度监测。

独立式烟雾探测器特点：1.独立探测、独立报警，不需和火灾报警控制器连接。2.安装便利，无需排线等工程。

![图片](https://mmbiz.qpic.cn/mmbiz_jpg/HqhlXMCNZHzR6A0BCYR0gJ0JAiaIIP31tZU8qplZp9vHRvxicmXnzXwfazabWAclAuiaaCdJMYuYs2h9OHzxAeibZQ/640?wx_fmt=jpeg&wxfrom=5&wx_lazy=1&wx_co=1)

![图片](https://mmbiz.qpic.cn/mmbiz_jpg/HqhlXMCNZHzR6A0BCYR0gJ0JAiaIIP31tUcdQR7y8FfUeW8uq70Yk4dNU3MKy9Ue3tsvmqufaCfV5CBzdwbEELQ/640?wx_fmt=jpeg&wxfrom=5&wx_lazy=1&wx_co=1)

![图片](https://mmbiz.qpic.cn/mmbiz_jpg/HqhlXMCNZHzR6A0BCYR0gJ0JAiaIIP31tbtiaegvAWuSq4r8HTfrrvZJ9wOHE1KhKjqUibh1pgevnUEx4tAzJtuiaQ/640?wx_fmt=jpeg&wxfrom=5&wx_lazy=1&wx_co=1)

![图片](https://mmbiz.qpic.cn/mmbiz_jpg/HqhlXMCNZHzR6A0BCYR0gJ0JAiaIIP31tMiaxT4vnwQvmpNfaNWvYzdIjc2JrvGZFsvX8AhiclwaSCkQfBhcav4ow/640?wx_fmt=jpeg&wxfrom=5&wx_lazy=1&wx_co=1)

![图片](https://mmbiz.qpic.cn/mmbiz_jpg/HqhlXMCNZHzR6A0BCYR0gJ0JAiaIIP31tImjibkwMT7Fibltn72hUoYF6A16Tvp0UJpHARv0h8FuWQej49BfQMiaibQ/640?wx_fmt=jpeg&wxfrom=5&wx_lazy=1&wx_co=1)

![图片](https://mmbiz.qpic.cn/mmbiz_jpg/HqhlXMCNZHzR6A0BCYR0gJ0JAiaIIP31tEgTCgZhKkIk3TgY6BHwX8pFU3BWS3GhV4jhTsibMrH5C3zE6ZqjgwaA/640?wx_fmt=jpeg&wxfrom=5&wx_lazy=1&wx_co=1)

![图片](https://mmbiz.qpic.cn/mmbiz_jpg/HqhlXMCNZHzR6A0BCYR0gJ0JAiaIIP31twdxZmeCODk12Z0k1JwZEibPCEnKHMGwic2m5R4nmxRh1LKGxiaOPl52MQ/640?wx_fmt=jpeg&wxfrom=5&wx_lazy=1&wx_co=1)

![图片](https://mmbiz.qpic.cn/mmbiz_jpg/HqhlXMCNZHzR6A0BCYR0gJ0JAiaIIP31tPcDhfxYRwGicpMsbXw2RhQ2Y9blIvRDeNWhib88of4ebkxCXezMYlWOg/640?wx_fmt=jpeg&wxfrom=5&wx_lazy=1&wx_co=1)

![图片](https://mmbiz.qpic.cn/mmbiz_jpg/HqhlXMCNZHzR6A0BCYR0gJ0JAiaIIP31tWYE3LvxulAFkx9yT997qJfiaYX5NchWfCpmWickBo7MEYNMGfN6eb2qA/640?wx_fmt=jpeg&wxfrom=5&wx_lazy=1&wx_co=1)

![图片](https://mmbiz.qpic.cn/mmbiz_jpg/HqhlXMCNZHzR6A0BCYR0gJ0JAiaIIP31tia5lMMbC5ic06ryvdWyBdOricO4rIWWicTxfVMXERtTI26huADEABqrfIQ/640?wx_fmt=jpeg&wxfrom=5&wx_lazy=1&wx_co=1)

![图片](https://mmbiz.qpic.cn/mmbiz_jpg/HqhlXMCNZHzR6A0BCYR0gJ0JAiaIIP31t1a9XFzLun4zDyo4mIBJpZfM0ibTf0q0VicnPSdptjHm1XVgZV2Lo0LPA/640?wx_fmt=jpeg&wxfrom=5&wx_lazy=1&wx_co=1)

![图片](https://mmbiz.qpic.cn/mmbiz_jpg/HqhlXMCNZHzR6A0BCYR0gJ0JAiaIIP31tPXF94OBlMay14u6iaHTsM0JQQ48pwZ7M6c6Wia5UVoib4H0cl1yQVuu2Q/640?wx_fmt=jpeg&wxfrom=5&wx_lazy=1&wx_co=1)

![图片](https://mmbiz.qpic.cn/mmbiz_jpg/HqhlXMCNZHzR6A0BCYR0gJ0JAiaIIP31tbdYuSqaI21O7eTmqsiaWtdLzJpXRDRQWtjjvx5EibUCJ6H1UPI7T1wOA/640?wx_fmt=jpeg&wxfrom=5&wx_lazy=1&wx_co=1)